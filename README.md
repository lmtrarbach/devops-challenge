# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Its a challenge for avenue code

### How do I get set up? ###

Puppet code for tomcat deploy with war file

### Depedencies###
Your puppet master should use this modules?

puppetlabs/tomcat
puppetlabs/stdlib
puppetlabs/vcsrepo

######Docker files#########

###########Tomcat######################
FROM centos
RUN yum -y update
RUN yum -y install wget
# Downloading Java
RUN wget --no-cookies --no-check-certificate --header "Cookie: oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/$JAVA_VERSION-$BUILD_VERSION/jdk-$JAVA_VERSION-linux-x64.rpm" -O /tmp/jdk-8-linux-x64.rpm

RUN yum -y install /tmp/jdk-8-linux-x64.rpm
RUN rpm -ivh https://yum.puppetlabs.com/puppetlabs-release-pc1-el-7.noarch.rpm
RUN yum -y install puppet-agent
RUN /opt/puppetlabs/bin/puppet resource service puppet ensure=running enable=true
COPY hosts /etc/hosts
# Expose the default tomcat port
EXPOSE 8080
RUN /opt/puppetlabs/bin/puppet agent -t --server (Name of thee puppet server)


##################Puppet master####################################
FROM centos
RUN yum -y update
RUN yum -y install wget

# Downloading Java
RUN wget --no-cookies --no-check-certificate --header "Cookie: oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/$JAVA_VERSION-$BUILD_VERSION/jdk-$JAVA_VERSION-linux-x64.rpm" -O /tmp/jdk-8-linux-x64.rpm

RUN yum -y install /tmp/jdk-8-linux-x64.rpm
RUN rpm -ivh https://yum.puppetlabs.com/puppetlabs-release-pc1-el-7.noarch.rpm
RUN yum -y update
RUN yum -y install puppetserver
RUN yum -y install git
# Expose the default puppet master port
EXPOSE 8140
RUN systemctl start puppetserver
RUN opt/puppetlabs/bin/puppet  module install puppetlabs-tomcat --version 1.0.1
RUN opt/puppetlabs/bin/puppet  module upgrade puppetlabs-stdlib
RUN opt/puppetlabs/bin/puppet module install puppetlabs-vcsrepo --version 1.0.2
RUN git clone https://lmtrarbach@bitbucket.org/lmtrarbach/devops-challenge.git 
RUN cp -Rf devops-challenge  /etc/puppetlabs/code/environments/production/manifests/
#####################################################################################