node 'default'
        {

        vcsrepo { '/tmp/tomcat-war-avenue.git':
        ensure   => present,
        provider => git,
        source   => 'git@bitbucket.org:lmtrarbach/tomcat-war-avenue.git',
        }->


        tomcat::install { '/opt/tomcat9':
        source_url => 'https://www.apache.org/dist/tomcat/tomcat-9/v9.0.8/bin/apache-tomcat-9.0.8.tar.gz',
        }->
        tomcat::service { 'default':
        catalina_base => '/opt/tomcat9',
        }->
        tomcat::war { 'java-chef-test.war':
        catalina_base => '/opt/tomcat9/',
        war_source    => '/tmp/tomcat-war-avenue.git/java-chef-test.war',
        }

}
